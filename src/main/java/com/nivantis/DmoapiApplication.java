package com.nivantis;

import org.apache.catalina.Context;
import org.apache.catalina.connector.Connector;
import org.apache.tomcat.util.descriptor.web.SecurityCollection;
import org.apache.tomcat.util.descriptor.web.SecurityConstraint;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.nivantis.entity.Calculatrice;
import com.nivantis.entity.Client;
import com.nivantis.entity.Laboratoire;
import com.nivantis.entity.Pharmacie;
import com.nivantis.entity.Produit;
import com.nivantis.repository.CalculatriceRepository;
import com.nivantis.repository.ClientRepository;
import com.nivantis.repository.LaboratoireRepository;
import com.nivantis.repository.PharmacieRepository;
import com.nivantis.repository.ProduitRepository;

//@SpringBootApplication(scanBasePackages = { "com.nivantis.entity", "com.nivantis.repository", "com.nivantis.controller","com.nivantis.jpa.exception"})
//@EnableAutoConfiguration
@SpringBootApplication
@EnableJpaAuditing
@EntityScan("com.nivantis.entity")
@EnableJpaRepositories(basePackages = {"com.nivantis.repository"})
@ComponentScan(basePackages= {"com.nivantis.controller"})
public class DmoapiApplication implements CommandLineRunner{
	
	@Autowired
	private ClientRepository clientRepository;
	@Autowired
	private LaboratoireRepository laboratoireRepository;
	@Autowired
	private PharmacieRepository pharmacieRepository;
	@Autowired
	private ProduitRepository produitRepository;
	@Autowired
	private CalculatriceRepository calculatriceRepository;

	public static void main(String[] args) {
		SpringApplication.run(DmoapiApplication.class, args);
		
	}

	@Override
	public void run(String... args) throws Exception
	{
		
		 //Cleanup the tables
        clientRepository.deleteAllInBatch();
        laboratoireRepository.deleteAllInBatch();
        this.pharmacieRepository.deleteAllInBatch();
        this.produitRepository.deleteAllInBatch();
        calculatriceRepository.deleteAllInBatch();
        
        
		Laboratoire L1=new Laboratoire("labovision","16 rue quai lahmar",
						"92000","labovision@gmail.com",
						"0146666666");
		
		Calculatrice calc1 =new Calculatrice("12.50","0.15","9.0","9.5","0.5");
		
		Calculatrice calc2 =new Calculatrice("12.50","0.15","9.0","9.5","0.5");
		
		Produit pr1=new Produit("25.42","48.12","effaralgantino","250","labovision","3");
		Produit pr2= new Produit("23.42","46.12","Héglantine","25","labovision","3");
		
		pr1.setLaboratoire(L1);		
		pr2.setLaboratoire(L1);

		// Add comments in the Post
		L1.getProduits().add(pr1);
		L1.getProduits().add(pr2);

		// Save Post and Comments via the Post entity
		laboratoireRepository.save(L1);

		
		Pharmacie ph1=new Pharmacie("Hautspharmaseine","25 rue hauts de seine",
				"92000","0142411125","pharma.hautsdeseine@hotmail.fr",
				"2","Distillation de produits");
		
		Pharmacie ph2=new Pharmacie("pharmaicone","2 rue hauts des oliviers",
				"92150","0142451125","pharmaicone@gmail.com",
				"1","Phase de conservation du produit");
		Client C1=new Client("Johnson","WILLIAMS","johnson.williams@gmail.com",
				"0625453060");
		
		Client C2=new Client("Adele","LAHMAR","adele.lahmar@gmail.com",
				"0636721644");

		Client C3=new Client("Kevin","ORTUNOVSKY","kevin.ortunovsky@gmail.com",
				"0641721644");	
				
		C1.setPharmacie(ph1);
		C2.setPharmacie(ph1);
		C3.setPharmacie(ph2);

		// Add comments in the Post
		ph1.getClients().add(C1);
		ph1.getClients().add(C2);
		ph2.getClients().add(C3);
		
		// Save Post and Comments via the Post entity
		pharmacieRepository.save(ph1);
		pharmacieRepository.save(ph2);

		
		this.pharmacieRepository.save(ph1);
		this.pharmacieRepository.save(ph2);
		
		ph1.getProduits().add(pr1);
		ph1.getProduits().add(pr2);
		
//		pr1.getPharmacies().add(ph1);
//		pr2.getPharmacies().add(ph1);
		
		ph2.getProduits().add(this.produitRepository.getOne(1L));

		
		//pr1.getPharmacies().add(this.pharmacieRepository.getOne(2L));
		
		this.pharmacieRepository.save(ph1);
		this.pharmacieRepository.save(ph2);
					

		// Set parent reference(user) in child entity(userProfile)
		pr1.setCalculatrice(calc1);
								
		// Set child reference(userProfile) in parent entity(user)
		calc1.setProduit(this.produitRepository.getOne(1L));
		
		// Save Parent Reference (which will save the child as well)
		produitRepository.save(pr1);
		
		// Set parent reference(user) in child entity(userProfile)
		pr2.setCalculatrice(calc2);
								
		// Set child reference(userProfile) in parent entity(user)
		calc2.setProduit(this.produitRepository.getOne(2L));
		
		// Save Parent Reference (which will save the child as well)
		produitRepository.save(pr2);
				
		
		
		
		 

		
		// Delete developer id 1
	     // this.produitRepository.deleteById(1L);

	     // Delete project id 2
	     //this.pharmacieRepository.deleteById(2L);

		
		// Create a couple of Book and Publisher
        //pharmacieRepository.save(new Pharmacie("Hautspharmaseine",25,"rue hauts de seine",
		//92000,0142411125,"pharma.hautsdeseine@hotmail.fr",
		//2,"Distillation de produits", new Produit(L1,25.42,48.12,
		//"effaralgantino",250,"labovision",3), new Produit(L1,25.42,48.12,
		//"effaralgantino",250,"labovision",3)));
		
	}
	
}
