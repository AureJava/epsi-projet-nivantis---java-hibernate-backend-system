package com.nivantis.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nivantis.entity.Produit;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long>{
	
	Page<Produit> findByLaboratoireId(Long laboratoireId, Pageable pageable);
    Optional<Produit> findByIdAndLaboratoireId(Long id, Long laboratoireId);
	//Produit findById(Long id, Long laboratoireId, Pageable pageable);
//	Page<Produit> findByPharmacieId(Long pharmaId, Pageable pageable);
//    Optional<Produit> findByIdAndPharmacieId(Long id, Long laboratoireId);
	
    //List<Produit> findByName(String name);
}
