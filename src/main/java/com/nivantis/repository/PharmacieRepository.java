package com.nivantis.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nivantis.entity.Pharmacie;
import com.nivantis.entity.Produit;

@Repository
public interface PharmacieRepository extends JpaRepository<Pharmacie, Long>{
	
//	@Transactional(readOnly = true)
//	Page<Pharmacie> findByProduitId(Long produitId, Pageable pageable);
//	@Transactional(readOnly = true)
//    Optional<Pharmacie> findByIdAndProduitId(Long id, Long produitId);
	
//	  @Query("SELECT p FROM Pharmacie p WHERE p.produit.id=:produitId")
//	  default Page<Pharmacie> findByProduitId(Long produitId) {
//	    return findByProduitId(produitId, new PageRequest(0,10));
//	  }
}
