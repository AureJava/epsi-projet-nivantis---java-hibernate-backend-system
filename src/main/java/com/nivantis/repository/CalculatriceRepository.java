package com.nivantis.repository;

import java.util.Optional;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.nivantis.entity.Calculatrice;

@Repository
public interface CalculatriceRepository extends JpaRepository<Calculatrice,Long>{

//	Optional<Calculatrice> findById(Long id);
//
//	Calculatrice findById(Long calculatriceId, Long produitId, Pageable pageable);

}
