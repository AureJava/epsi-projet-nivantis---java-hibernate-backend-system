package com.nivantis.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name="pharmacies")
public class Pharmacie extends AuditModel{

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	@NotNull
    @Size(max = 250)
	@Column(name="nom_pharmacie")
	private String nom_pharmacie;
	
	
	@NotNull
    @Size(max = 250)
	@Column(name="adresse")
	private String adresse;
	
	@NotNull
    @Size(max = 250)
	@Column(name="code_postale")
	private String code_postale;
	
	@NotNull
	@Size(max = 250)
	@Column(name="num_tel")
	private String num_tel;
	
	@NotNull
    @Size(max = 250)
	@Column(name="email")
	private String email;
	
	@ManyToMany(fetch = FetchType.LAZY,cascade = {CascadeType.PERSIST,CascadeType.MERGE})
	@JoinTable(name = "pharmacie_produits", 
	joinColumns = { @JoinColumn(name = "pharmacie_id")}, 
	inverseJoinColumns = { @JoinColumn(name = "produit_id")})
	@JsonManagedReference
	@OnDelete(action = OnDeleteAction.CASCADE)
	Set<Produit> produits = new HashSet<>();
	
	@NotNull
    @Size(max = 250)
	@Column(name="nbre_achats")
	private String nbreachats;
	
	@NotNull
    @Size(max = 250)
	@Column(name="besoin_en_formation")
	private String besoinenformation;
	
	 @OneToMany(cascade = CascadeType.ALL,
	            fetch = FetchType.LAZY,
	            mappedBy = "pharmacie")
	private Set<Client> clients = new HashSet<>();
	 
	 public Pharmacie() {
		 
	 }

	public Pharmacie(@NotNull @Size(max = 250) String nom_pharmacie, 
			@NotNull @Size(max = 250) String adresse, @NotNull @Size(max = 250) String code_postale,
			@NotNull @Size(max = 250) String num_tel, @NotNull @Size(max = 250) String email,@NotNull @Size(max = 250) String nbreachats,
			@NotNull @Size(max = 250) String besoinenformation) {
		super();
		this.nom_pharmacie = nom_pharmacie;
		this.adresse = adresse;
		this.code_postale = code_postale;
		this.num_tel = num_tel;
		this.email = email;
		this.nbreachats = nbreachats;
		this.besoinenformation = besoinenformation;
	}
	
//	public Pharmacie(@NotNull @Size(max = 250) String nom_pharmacie, @NotNull int num_rue,
//			@NotNull @Size(max = 250) String nom_rue, @NotNull @Size(max = 250) int code_postale,
//			@NotNull int num_tel, @NotNull @Size(max = 250) String email,
//			int nbreachats,
//			@NotNull @Size(max = 250) String besoinenformation,
//			Produit... produits) {
//        super();
//		this.nom_pharmacie = nom_pharmacie;
//		this.num_rue = num_rue;
//		this.nom_rue = nom_rue;
//		this.code_postale = code_postale;
//		this.num_tel = num_tel;
//		this.email = email;
//		this.nbreachats = nbreachats;
//		this.besoinenformation = besoinenformation;
//        this.produits = Stream.of(produits).collect(Collectors.toSet());
//        this.produits.forEach(x -> x.getPharmacies().add(this));
//	}



	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public String getNom_pharmacie() {
		return nom_pharmacie;
	}

	public void setNom_pharmacie(String nom_pharmacie) {
		this.nom_pharmacie = nom_pharmacie;
	}	

	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCode_postale() {
		return code_postale;
	}

	public void setCode_postale(String code_postale) {
		this.code_postale = code_postale;
	}

	public String getNum_tel() {
		return num_tel;
	}

	public void setNum_tel(String num_tel) {
		this.num_tel = num_tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Set<Produit> getProduits() {
		return produits;
	}

	public void setProduits(Set<Produit> produits) {
		this.produits = produits;
	}

	public String getNbreachats() {
		return nbreachats;
	}

	public void setNbreachats(String nbreachats) {
		this.nbreachats = nbreachats;
	}

	public String getBesoinenformation() {
		return besoinenformation;
	}

	public void setBesoinenformation(String besoinenformation) {
		this.besoinenformation = besoinenformation;
	}

	public Set<Client> getClients() {
		return clients;
	}

	public void setClients(Set<Client> clients) {
		this.clients = clients;
	}
	
}
