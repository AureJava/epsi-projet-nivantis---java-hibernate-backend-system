package com.nivantis.entity;

import java.io.Serializable;
import java.util.Optional;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;

//@EnableJpaRepositories
@Entity
@Table(name="clients")
public class Client extends AuditModel{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	

	@ManyToOne(fetch=FetchType.LAZY)
	@JoinColumn(name="pharmacie_id",nullable = false)
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("post_id")
	private Pharmacie pharmacie;
	@NotNull
	@Size(max = 250)
	@Column(name="prenom")
	private String prenom;
	@NotNull
	@Size(max = 250)
	@Column(name="nom")
	private String nom;
	@NotNull
	@Size(max = 250)
	@Column(name="email")
	private String email;
	@NotNull
	@Size(max = 250)
	@Column(name="numtel")
	private String numero_tel;
	
	public Client() {
		
	}
	

	public Client( @NotNull @Size(max = 250) String prenom, @NotNull @Size(max = 250) String nom,
			@NotNull @Size(max = 250) String email, String numero_tel) {
		super();
		this.prenom = prenom;
		this.nom = nom;
		this.email = email;
		this.numero_tel = numero_tel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long Id) {
		this.id = Id;
	}

	public Pharmacie getPharmacie() {
		return pharmacie;
	}

	public void setPharmacie(Pharmacie pharmacie) {
		this.pharmacie = pharmacie;
	}

	public String getPrenom() {
		return prenom;
	}

	public void setPrenom(String prenom) {
		this.prenom = prenom;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNumero_tel() {
		return numero_tel;
	}

	public void setNumero_tel(String numero_tel) {
		this.numero_tel = numero_tel;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}	
	
	
}
