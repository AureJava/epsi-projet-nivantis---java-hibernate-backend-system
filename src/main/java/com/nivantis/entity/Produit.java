package com.nivantis.entity;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIdentityReference;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import com.nivantis.entity.Pharmacie;

//@EnableJpaRepositories
@Entity
@Table(name="produits")
public class Produit extends AuditModel implements Serializable{
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	
	//jointure avec la cl� �trang�re que tu appelles comme tu veux
	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name="laboratoire_id")
	@JsonIdentityInfo(generator=ObjectIdGenerators.PropertyGenerator.class, property="id")
    @JsonIdentityReference(alwaysAsId=true)
    @JsonProperty("post_id")
	private Laboratoire laboratoire;
	//@Transactional
	//only this is cascading
	@ManyToMany(
          fetch = FetchType.LAZY,
          cascade = {CascadeType.PERSIST,CascadeType.MERGE},
          mappedBy = "produits"
  )
  @OnDelete(action = OnDeleteAction.CASCADE)
  private Set<Pharmacie> pharmacies = new HashSet<>();
	
	@OneToOne(fetch = FetchType.LAZY,
          cascade =  CascadeType.ALL,
          mappedBy = "produit")
	private Calculatrice calculatrice;
	
	@NotNull
	@Size(max = 250)
	@Column(name="coor_x")
	private String coordonnees_x;
	
	@NotNull
	@Size(max = 250)
	@Column(name="coor_y")
	private String coordonnees_y;

	@NotNull
  @Size(max = 250)
	@Column(name="nom_produit")
	private String nom;
	
	@NotNull
	@Size(max = 250)
	@Column(name="prix_produit")
	private String prix;
	
	@NotNull
  @Size(max = 250)
	@Column(name="nom_laboratoire")
	private String nom_laboratoire;
	
	@NotNull
	@Size(max = 250)
	@Column(name="nbre_achats")
	private String nbrachats;
	
	public Produit()
	{
		
	}

	
	public Produit(@NotNull @Size(max = 250)String coordonnees_x,
			@NotNull @Size(max = 250) String coordonnees_y, @NotNull @Size(max = 250) String nom,@NotNull
			@Size(max = 250) String prix,
			@NotNull @Size(max = 250) String nom_laboratoire, @NotNull
			@Size(max = 250)String nbrachats) {
		super();
		this.coordonnees_x = coordonnees_x;
		this.coordonnees_y = coordonnees_y;
		this.nom = nom;
		this.prix = prix;
		this.nom_laboratoire = nom_laboratoire;
		this.nbrachats = nbrachats;
	}


	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Laboratoire getLaboratoire() {
		return laboratoire;
	}

	public void setLaboratoire(Laboratoire laboratoire) {
		this.laboratoire = laboratoire;
	}

	public Set<Pharmacie> getPharmacies() {
		return pharmacies;
	}

	public void setPharmacies(Set<Pharmacie> pharmacies) {
		this.pharmacies = pharmacies;
	}
	
	

	public Calculatrice getCalculatrice() {
		return calculatrice;
	}

	public void setCalculatrice(Calculatrice calculatrice) {
		this.calculatrice = calculatrice;
	}

	public String getCoordonnees_x() {
		return coordonnees_x;
	}

	public void setCoordonnees_x(String coordonnees_x) {
		this.coordonnees_x = coordonnees_x;
	}
	
	public String getCoordonnees_y() {
		return coordonnees_y;
	}

	public void setCoordonnees_y(String coordonnees_y) {
		this.coordonnees_y = coordonnees_y;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getPrix() {
		return prix;
	}

	public void setPrix(String prix) {
		this.prix = prix;
	}

	public String getNom_laboratoire() {
		return nom_laboratoire;
	}

	public void setNom_laboratoire(String nom_laboratoire) {
		this.nom_laboratoire = nom_laboratoire;
	}

	public String getNbrachats() {
		return nbrachats;
	}

	public void setNbrachats(String nbrachats) {
		this.nbrachats = nbrachats;
	}
	
	
	
}
