package com.nivantis.entity;

import java.io.Serializable;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

//@EnableJpaRepositories
@Entity
@Table(name="laboratoires")
public class Laboratoire extends AuditModel{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	//association bidirectionnel
	//classe operation j'ai un objet de type compte
	@OneToMany(cascade = CascadeType.ALL,
          fetch = FetchType.LAZY,
          mappedBy="laboratoire")
	private Set<Produit> produits = new HashSet<>();
	
	
	@NotNull
	@Size(max = 250)
	@Column(name="Nom_laboratoire")
	private String nom_laboratoire;
	
	@NotNull
	@Size(max = 250)
	@Column(name="Adresse")
	private String adresse;
	
	@NotNull
	@Size(max = 250)
	@Column(name="Code_postale")
	private String codepostale;
	
	@NotNull
	@Size(max = 250)
	@Column(name="Email")
	private String email;
	
	@NotNull
	@Size(max = 250)
	@Column(name="Numero_tel")
	private String numero_tel;


	public Laboratoire() {
		
	}

	public Laboratoire(@NotNull @Size(max = 250) String nom_laboratoire, 
			@NotNull @Size(max = 250) String adresse, @NotNull @Size(max = 250) String codepostale,
			@NotNull @Size(max = 250) String email,
			@NotNull @Size(max = 250) String numero_tel) {
		super();
		this.nom_laboratoire = nom_laboratoire;
		this.adresse = adresse;
		this.codepostale = codepostale;
		this.email = email;
		this.numero_tel = numero_tel;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}
	
	public Set<Produit> getProduits() {
		return produits;
	}

	public void setProduits(Set<Produit> produits) {
		this.produits = produits;
	}

	public String getNom_laboratoire() {
		return nom_laboratoire;
	}

	public void setNom_laboratoire(String nom_laboratoire) {
		this.nom_laboratoire = nom_laboratoire;
	}


	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getCodepostale() {
		return codepostale;
	}

	public void setCodepostale(String codepostale) {
		this.codepostale = codepostale;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNumero_tel() {
		return numero_tel;
	}

	public void setNumero_tel(String numero_tel) {
		this.numero_tel = numero_tel;
	}
	
}
