package com.nivantis.entity;

import java.io.Serializable;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

//@EnableJpaRepositories
@Entity
@Table(name="calculatrices")
public class Calculatrice extends AuditModel implements Serializable{
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Long id;
	
	
	@OneToOne(fetch=FetchType.LAZY,optional=false)
	@JoinColumn(name="produit_id",nullable=false)
	private Produit produit;
	
	@Column(name="prix_achat_brut")
	private String prix_achat_brut;
	
	@Column(name="tauxremise")
	private String tauxremise;//=(1-(prix_achat_net/prix_achat_brut))*100;
	
	@Column(name="prix_achat_net")
	private String prix_achat_net;//=prix_achat_brut*(1-tauxremise);
	
	@Column(name="prix_vente_net")
	private String prix_vente_net;//=prix_achat_net*coeffmulti;
	
	@Column(name="coeffmulti")
	private String coeffmulti;//=prix_vente_net/prix_achat_net;
	
	

	public Calculatrice() {
		
	}


	public Calculatrice(String prix_achat_brut, String tauxremise,
			String prix_achat_net, String prix_vente_net, String coeffmulti) {
		super();
		this.prix_achat_brut = prix_achat_brut;
		this.tauxremise = tauxremise;
		this.prix_achat_net = prix_achat_net;
		this.prix_vente_net = prix_vente_net;
		this.coeffmulti = coeffmulti;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Produit getProduit() {
		return produit;
	}

	public void setProduit(Produit produit) {
		this.produit = produit;
	}

	public String getPrix_achat_brut() {
		return prix_achat_brut;
	}

	public void setPrix_achat_brut(String prix_achat_brut) {
		this.prix_achat_brut = prix_achat_brut;
	}

	public String getTauxremise() {
		return tauxremise;
	}

	public void setTauxremise(String tauxremise) {
		this.tauxremise = tauxremise;
	}

	public String getPrix_achat_net() {
		return prix_achat_net;
	}

	public void setPrix_achat_net(String prix_achat_net) {
		this.prix_achat_net = prix_achat_net;
	}

	public String getPrix_vente_net() {
		return prix_vente_net;
	}

	public void setPrix_vente_net(String prix_vente_net) {
		this.prix_vente_net = prix_vente_net;
	}

	public String getCoeffmulti() {
		return coeffmulti;
	}

	public void setCoeffmulti(String coeffmulti) {
		this.coeffmulti = coeffmulti;
	}
}
