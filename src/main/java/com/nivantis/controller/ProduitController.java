package com.nivantis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nivantis.entity.Client;
import com.nivantis.entity.Produit;
import com.nivantis.jpa.exception.ResourceNotFoundException;
import com.nivantis.repository.ClientRepository;
import com.nivantis.repository.LaboratoireRepository;
import com.nivantis.repository.PharmacieRepository;
import com.nivantis.repository.ProduitRepository;

@RestController
@RequestMapping("/produits")
public class ProduitController {
	@Autowired
	private ProduitRepository produitRepository;
	
	@Autowired
	private LaboratoireRepository laboratoireRepository;
	
	@Autowired
	private PharmacieRepository pharmacieRepository;
	 
	@GetMapping("/all")
	public Iterable<Produit> getProduits(){
		return produitRepository.findAll();
	}
	
//	@GetMapping("/laboratoires/{laboratoireId}/{produitId}")
//    public Produit getProduit(
//    		@PathVariable (value = "produitId") Long produitId,
//    		@PathVariable (value = "laboratoireId") Long laboratoireId,
//                                                Pageable pageable) {
//        return produitRepository.findById(produitId,laboratoireId,pageable);
//    }
	
	@GetMapping("/produit/{produitId}")
    public  Produit getProduit(
    		@PathVariable (value = "produitId") Long produitId) {
        return produitRepository.getOne(produitId);
    }
	
	@GetMapping("/laboratoires/{laboratoireId}/produits")
    public Page<Produit> getAllProduitsByLaboratoireId
    (@PathVariable (value = "laboratoireId") Long laboratoireId,
     Pageable pageable) {
        return produitRepository.findByLaboratoireId(laboratoireId, pageable);
    }
	
//	@GetMapping("/pharmacies/{pharmaId}/produits")
//    public Page<Produit> getAllProduitsByPharmacieId
//    (@PathVariable (value = "pharmaId") Long pharmaId,
//     Pageable pageable) {
//        return produitRepository.findByPharmacieId(pharmaId, pageable);
//    }


    @PostMapping("/laboratoires/{labortoireId}/produits")
    public Produit createProduit(@PathVariable (value = "laboratoireId") Long laboratoireId,
    @Valid @RequestBody Produit produit) {
        return laboratoireRepository.findById(laboratoireId).map(laboratoire -> {
            produit.setLaboratoire(laboratoire);
            return produitRepository.save(produit);
        }).orElseThrow(() -> new ResourceNotFoundException("LaboratoireId " + laboratoireId + " pas trouv�"));
    }

    @PutMapping("/laboratoires/{laboratoireId}/produits/{produitId}")
    public Produit updateProduit(@PathVariable (value = "laboratoireId") Long laboratoireId,
    @PathVariable (value = "produitId") Long produitId,
    @Valid @RequestBody Produit produitRequest) {
        if(!laboratoireRepository.existsById(laboratoireId)) {
            throw new ResourceNotFoundException("LaboratoireId " + laboratoireId + " pas trouv�");
        }

        return produitRepository.findById(produitId).map(produit -> {
        	produit.setLaboratoire(produitRequest.getLaboratoire());
        	produit.setPharmacies(produitRequest.getPharmacies());
        	produit.setCalculatrice(produitRequest.getCalculatrice());
        	produit.setCoordonnees_x(produitRequest.getCoordonnees_x());
        	produit.setCoordonnees_y(produitRequest.getCoordonnees_y());
        	produit.setNom(produitRequest.getNom());
        	produit.setPrix(produitRequest.getPrix());
        	produit.setNom_laboratoire(produitRequest.getNom_laboratoire());
        	produit.setNbrachats(produitRequest.getNbrachats());
        	return produitRepository.save(produit);
        }).orElseThrow(() -> new ResourceNotFoundException("ProduitId " + produitId + "pas trouv�"));
    }

    @DeleteMapping("/laboratoires/{laboratoireId}/produits/{produitId}")
    public ResponseEntity<?> deleteProduitinLaboratoire(@PathVariable (value = "laboratoireId") Long laboratoireId,
                              @PathVariable (value = "produitId") Long produitId) {
        return produitRepository.findByIdAndLaboratoireId(produitId, laboratoireId).map(produit -> {
            produitRepository.delete(produit);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Produit pas trouv� avec Id " + produitId + " et laboratoireId " + laboratoireId));
    }
    
//    @DeleteMapping("/pharmacies/{pharmaId}/produits/{produitId}")
//    public ResponseEntity<?> deleteProduitinPharmacie(@PathVariable (value = "produitId") Long produitId,
//                              @PathVariable (value = "pharmaId") Long pharmaId) {
//        return produitRepository.findByIdAndPharmacieId(pharmaId,produitId).map(produit -> {
//            produitRepository.delete(produit);
//            return ResponseEntity.ok().build();
//        }).orElseThrow(() -> new ResourceNotFoundException("Produit pas trouv� avec Id " + produitId + " et pharmaId " + pharmaId));
//    }
}

