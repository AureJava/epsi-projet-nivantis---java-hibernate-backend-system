package com.nivantis.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nivantis.entity.Client;
import com.nivantis.jpa.exception.ResourceNotFoundException;
import com.nivantis.repository.ClientRepository;
import com.nivantis.repository.PharmacieRepository;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.util.UriComponentsBuilder;

@RestController
@RequestMapping("/clients")
public class ClientController {
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private PharmacieRepository pharmacieRepository;
	 
	@GetMapping("/all")
	public Iterable<Client> getClients(){
		return clientRepository.findAll();
	}
	
	@GetMapping("/pharmacies/{pharmaId}/clients")
    public Page<Client> getAllClientsByPharmacieId(@PathVariable (value = "pharmaId") Long pharmaId,
                                                Pageable pageable) {
        return clientRepository.findByPharmacieId(pharmaId, pageable);
    }
	
//	@GetMapping("/pharmacie/{pharmaId}/{clientId}")
//    public Client getClient(
//    		@PathVariable (value = "clientId") Long clientId,
//    		@PathVariable (value = "pharmaId") Long pharmaId,
//                                                Pageable pageable) {
//        return clientRepository.findById(clientId,pharmaId,pageable);
//    }
	@GetMapping("/client/{clientId}")
    public  Client getClient(
    		@PathVariable (value = "clientId") Long clientId) {
        return clientRepository.getOne(clientId);
    }

    @PostMapping("/pharmacies/{pharmaId}/client")
    public Client createClient(@PathVariable (value = "pharmaId") Long pharmaId,
                                 @Valid @RequestBody Client client) {
        return pharmacieRepository.findById(pharmaId).map(pharmacie -> {
            client.setPharmacie(pharmacie);
            return clientRepository.save(client);
        }).orElseThrow(() -> new ResourceNotFoundException("PharmacieId " + pharmaId + " pas trouv�"));
    }

    @PutMapping("/pharmacies/{pharmaId}/clients/{clientId}")
    public Client updateClient(@PathVariable (value = "pharmaId") Long pharmaId,
                                 @PathVariable (value = "clientId") Long clientId,
                                 @Valid @RequestBody Client clientRequest) {
        if(!pharmacieRepository.existsById(pharmaId)) {
            throw new ResourceNotFoundException("PharmaId " + pharmaId + " pas trouv�");
        }

        return clientRepository.findById(clientId).map(client -> {
        	 client.setPharmacie(clientRequest.getPharmacie());
        	client.setPrenom(clientRequest.getPrenom());
            client.setNom(clientRequest.getNom());
            client.setEmail(clientRequest.getEmail());
            client.setNumero_tel(clientRequest.getNumero_tel());
            return clientRepository.save(client);
        }).orElseThrow(() -> new ResourceNotFoundException("ClientId " + clientId + "pas trouv�"));
    }

    @DeleteMapping("/pharmacies/{pharmaId}/clients/{clientId}")
    public ResponseEntity<?> deleteClient(@PathVariable (value = "pharmaId") Long pharmaId,
                              @PathVariable (value = "clientId") Long clientId) {
        return clientRepository.findByIdAndPharmacieId(clientId, pharmaId).map(client -> {
            clientRepository.delete(client);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("Client pas trouv� avec Id " + clientId + " et pharmacieId " + pharmaId));
    }
}
 




