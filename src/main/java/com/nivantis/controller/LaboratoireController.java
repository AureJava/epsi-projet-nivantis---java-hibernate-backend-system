package com.nivantis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nivantis.entity.Laboratoire;
import com.nivantis.entity.Pharmacie;
import com.nivantis.jpa.exception.ResourceNotFoundException;
import com.nivantis.repository.LaboratoireRepository;
import com.nivantis.repository.PharmacieRepository;

@RestController
@RequestMapping("/laboratoires")
public class LaboratoireController {
	
	 @Autowired
	    private LaboratoireRepository laboratoireRepository;

	    @GetMapping("/all")
	    public Page<Laboratoire> getAllLaboratoire(Pageable pageable) {
	        return laboratoireRepository.findAll(pageable);
	    }

	    @PostMapping("/laboratoire")
	    public Laboratoire createLaboratoire(@Valid @RequestBody Laboratoire laboratoire) {
	        return laboratoireRepository.save(laboratoire);
	    }

	    @PutMapping("/laboratoire/{laboratoireId}")
	    public Laboratoire updateLaboratoire(@PathVariable Long laboratoireId, 
	    		@Valid @RequestBody Laboratoire laboratoireRequest) {
	        return laboratoireRepository.findById(laboratoireId).map(laboratoire -> {
	            laboratoire.setProduits(laboratoireRequest.getProduits());
	            laboratoire.setNom_laboratoire(laboratoireRequest.getNom_laboratoire());
	            laboratoire.setAdresse(laboratoireRequest.getAdresse());
	            laboratoire.setCodepostale(laboratoireRequest.getCodepostale());
	            laboratoire.setEmail(laboratoireRequest.getEmail());
	            laboratoire.setNumero_tel(laboratoireRequest.getNumero_tel());
	            return laboratoireRepository.save(laboratoire);
	        }).orElseThrow(() -> new ResourceNotFoundException("LaboratoireId " + laboratoireId + " pas trouv�"));
	    }


	    @DeleteMapping("/laboratoire/{laboratoireId}")
	    public ResponseEntity<?> deleteLaboratoire(@PathVariable Long laboratoireId) {
	        return laboratoireRepository.findById(laboratoireId).map(laboratoire -> {
	            laboratoireRepository.delete(laboratoire);
	            return ResponseEntity.ok().build();
	        }).orElseThrow(() -> new ResourceNotFoundException("LaboratoireId " + laboratoireId + " pas trouv�"));
	    }
}
