package com.nivantis.controller;

import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nivantis.entity.Calculatrice;
import com.nivantis.entity.Client;
import com.nivantis.entity.Laboratoire;
import com.nivantis.jpa.exception.ResourceNotFoundException;
import com.nivantis.repository.CalculatriceRepository;
import com.nivantis.repository.LaboratoireRepository;

//@Configuration
//@ComponentScan("com.nivantis.controller") 
//@Service
//@RequestMapping("/Calculatrice")
@RestController
@RequestMapping("/calculatrices")
public class CaculatriceController {
	@Autowired
  private CalculatriceRepository calculatriceRepository;

  @GetMapping("/all")
  public Page<Calculatrice> getAllCalculatrice(Pageable pageable) {
      return calculatriceRepository.findAll(pageable);
  }

  @PostMapping("/calculatrice")
  public Calculatrice createCalculatrice(@Valid @RequestBody Calculatrice calculatrice) {
      return calculatriceRepository.save(calculatrice);
  }
  
//  @GetMapping("/produit/{produitId}/{calculatriceId}")
//  public Calculatrice getCalculatrice(
//  		@PathVariable (value = "calculatriceId") Long calculatriceId,
//  		@PathVariable (value = "produitId") Long produitId,
//                                              Pageable pageable) {
//      return calculatriceRepository.findById(calculatriceId,produitId,pageable);
//  }
  
  @GetMapping("/calculatrice/{calculatriceId}")
  public  Calculatrice getOwnCalculatrice(
  		@PathVariable (value = "calculatriceId") Long calculatriceId) {
      return calculatriceRepository.getOne(calculatriceId);
  }

  @PutMapping("/calculatrice/{caluclatriceId}")
  public Calculatrice updateCalculatrice(@PathVariable Long calculatriceId, 
  		@Valid @RequestBody Calculatrice calculatriceRequest) {
      return calculatriceRepository.findById(calculatriceId).map(calculatrice -> {
          calculatrice.setProduit(calculatriceRequest.getProduit());
          calculatrice.setPrix_achat_brut(calculatriceRequest.getPrix_achat_brut());
          calculatrice.setTauxremise(calculatriceRequest.getTauxremise());
          calculatrice.setPrix_achat_net(calculatriceRequest.getPrix_achat_net());
          calculatrice.setPrix_vente_net(calculatriceRequest.getPrix_vente_net());
          calculatrice.setCoeffmulti(calculatriceRequest.getCoeffmulti());
          return calculatriceRepository.save(calculatrice);
      }).orElseThrow(() -> new ResourceNotFoundException("CalculatriceId " + calculatriceId + " pas trouv�"));
  }


  @DeleteMapping("/calculatrice/{calcualtriceId}")
  public ResponseEntity<?> deleteCalculatrice(@PathVariable Long calculatriceId) {
      return calculatriceRepository.findById(calculatriceId).map(calculatrice -> {
          calculatriceRepository.delete(calculatrice);
          return ResponseEntity.ok().build();
      }).orElseThrow(() -> new ResourceNotFoundException("CalculatriceId " + calculatriceId + " pas trouv�"));
  }
}
