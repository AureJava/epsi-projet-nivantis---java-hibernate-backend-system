package com.nivantis.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.nivantis.entity.Pharmacie;
import com.nivantis.entity.Produit;
import com.nivantis.jpa.exception.ResourceNotFoundException;
import com.nivantis.repository.PharmacieRepository;

@RestController
@RequestMapping("/pharmacies")
public class PharmacieController {

    @Autowired
    private PharmacieRepository pharmacieRepository;

    @GetMapping("/all")
    public Page<Pharmacie> getAllPharmacie(Pageable pageable) {
        return pharmacieRepository.findAll(pageable);
    }
    
//    @GetMapping("/produits/{produitId}/pharmacies")
//    public Page<Pharmacie> getAllPharmaciesByProduitId
//    (@PathVariable (value = "produitId") Long produitId,
//     Pageable pageable) {
//        return pharmacieRepository.findByProduitId(produitId, pageable);
//    }
    @GetMapping("/pharmacie/{pharmaId}")
    public  Pharmacie getPharmacie(
    		@PathVariable (value = "pharmaId") Long pharmaId) {
        return pharmacieRepository.getOne(pharmaId);
    }

    @PostMapping("/pharmacie")
    public Pharmacie createPharmacie(@Valid @RequestBody Pharmacie pharmacie) {
        return pharmacieRepository.save(pharmacie);
    }

    @PutMapping("/pharmacie/{pharmaId}")
    public Pharmacie updatePharmacie(@PathVariable Long pharmaId, @Valid @RequestBody Pharmacie pharmacieRequest) {
        return pharmacieRepository.findById(pharmaId).map(pharmacie -> {
        	pharmacie.setNom_pharmacie(pharmacieRequest.getNom_pharmacie());
        	pharmacie.setAdresse(pharmacieRequest.getAdresse());
        	pharmacie.setCode_postale(pharmacieRequest.getCode_postale());
        	pharmacie.setNum_tel(pharmacieRequest.getNum_tel());
        	pharmacie.setEmail(pharmacieRequest.getEmail());
            pharmacie.setProduits(pharmacieRequest.getProduits());
            pharmacie.setNbreachats(pharmacieRequest.getNbreachats());
            pharmacie.setBesoinenformation(pharmacieRequest.getBesoinenformation());
            pharmacie.setClients(pharmacieRequest.getClients());
            return pharmacieRepository.save(pharmacie);
        }).orElseThrow(() -> new ResourceNotFoundException("PharmacieId " + pharmaId + " pas trouv�"));
    }


    @DeleteMapping("/pharmacie/{pharmaId}")
    public ResponseEntity<?> deletePharmacie(@PathVariable Long pharmaId) {
        return pharmacieRepository.findById(pharmaId).map(pharmacie -> {
            pharmacieRepository.delete(pharmacie);
            return ResponseEntity.ok().build();
        }).orElseThrow(() -> new ResourceNotFoundException("PharmacieId " + pharmaId + " pas trouv�"));
    }
    
//    @DeleteMapping("/produits/{produitId}/pharmacies/{pharmaId}")
//    public ResponseEntity<?> deletePharmacieinProduit
//    (@PathVariable (value = "pharmaId") Long pharmaId,
//    @PathVariable (value = "produitId") Long produitId) {
//        return pharmacieRepository.findByIdAndProduitId(pharmaId,produitId).map(pharmacie -> {
//            pharmacieRepository.delete(pharmacie);
//            return ResponseEntity.ok().build();
//        }).orElseThrow(() -> new ResourceNotFoundException("Pharmacie pas trouv� avec Id " + pharmaId + " et produitId " + produitId));
//    }

}